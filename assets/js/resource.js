app.factory('TransactionService', ['$resource', function($resource) {
    return $resource('http://iv-777.dev.ivanhoe.io/api/transactions', null, {});
}]);

app.factory('WebsiteService', ['$resource', function($resource) {
    return $resource('http://iv-777.dev.ivanhoe.io/api/websites', null, {});
}]);

app.factory('AdvertisersService', ['$resource', function($resource) {
    return $resource('http://iv-777.dev.ivanhoe.io/api/advertisers', null, {});
}]);

app.factory('AffiliateNetworksService', ['$resource', function($resource) {
    return $resource('http://iv-777.dev.ivanhoe.io/api/affiliate_networks', null, {});
}]);